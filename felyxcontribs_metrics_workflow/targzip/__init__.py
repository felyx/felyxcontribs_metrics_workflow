# encoding: utf-8
"""
The targzip plugin
------------------

Produce a gzipped tar archive of all the input files. It accepts only the
results of a query step (or set of selection steps) and as such is a
query level plugin.

It utilises the QueryPlugin class.

.. :copyright: Copyright 2013 Pelamis Scientific Software Ltd.
.. :license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: David Poulter <david.poulter@pelamis.co.uk>
.. codeauthor:: David Poulter <david.poulter@pelamis.co.uk>
"""
import tarfile
import tempfile
import json
import os

from felyx_processor.query.finders import get_miniprod_path
from felyx_processor.processors.plugins import PluginMetadata, operator
from felyx_processor.processors.plugins.baseplugin import QueryPlugin


info = PluginMetadata(
    'targzip', 'TarGzip',
    ['has_task', 'perform', 'is_reducer', 'expects_uri']
)


class TarGzip(QueryPlugin):
    """Produce a tar gzip archive of all the files in the input source.
    Expects to be called from a query."""

    @staticmethod
    def has_task(task_id):
        """
        Return True if task_id is 'targzip'.

        :param str task_id: The id of the task to test for.
        :return: True if task_id matches this task.
        :rtype: bool
        """
        return task_id in ['targzip']

    # noinspection PyUnusedLocal
    @staticmethod
    @operator(
        'packaging', 1,
        {'type': dict, 'userDefined': False,
         'doc': 'A dictionary of input values'},
        {'type': str, 'userDefined': True,
         'doc': 'The path of the output file'}
    )
    def targzip(args=None, input_data=None, outfile=''):
        """
        Stores required data in a tgz file.

        :param args: Ignored arguments for compatibility.
        :param input_data: A dictionary of input values.
        :type input_data: dict or list
        :param str outfile: The path of the output file.
        """
        if input_data is None:
            input_data = args

        values, arguments, previous_step = QueryPlugin.get_inputs(
            input_data, 'packaging'
        )

        query_file = None

        miniprods = []
        paths = []


        # The results come from a selection, they will be a list of dicts:
        if isinstance(values, dict):
            values = [values]

        # build a list of all the miniprods
        for selection in values:
            for dataset_name, site_group in selection.items():
                if ':' in dataset_name:
                    instance = dataset_name.split(':')[0]
                else:
                    instance = None
                for site_name, miniprod_group in site_group.items():
                    for miniprod in miniprod_group['miniprods']:
                        miniprods.append([miniprod, instance])

        # build a list of the paths to the files
        paths = [get_miniprod_path(*_) for _ in miniprods]

        if not len(paths):
            return False

        with tarfile.open(outfile, mode='w:gz') as tar_out_file:
            for path in paths:
                if path is None:
                    continue
                if os.path.islink(path):
                    path = os.readlink(path)

                tar_out_file.add(name=path, arcname=os.path.basename(path))

            try:
                query = json.dumps(input_data)
                with tempfile.NamedTemporaryFile(delete=False) as query_file:
                    query_file.write(query)
                tar_out_file.add(
                    name=query_file.name,
                    arcname="query_result.json"
                )
                os.remove(query_file.name)
            except TypeError:
                pass

        return True
