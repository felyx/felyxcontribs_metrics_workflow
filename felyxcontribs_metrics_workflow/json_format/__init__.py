# encoding: utf-8
"""
The json_format plugin
------------------

This plugin stores the textual results of a query in a JSON format.
It accepts only the results of a query step (or set of selection steps) and as
such is a query level plugin.

It utilises the QueryPlugin class.

.. :copyright: Copyright 2014 Pelamis Scientific Software Ltd.
.. :license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: David Poulter <david.poulter@pelamis.co.uk>
.. codeauthor:: David Poulter <david.poulter@pelamis.co.uk>
"""
from felyx_work.processors.plugins import operator, PluginMetadata
from felyx_work.processors.plugins.baseplugin import QueryPlugin

info = PluginMetadata('json_format', 'JSONFormat',
                      ['has_task', 'perform', 'is_reducer', 'expects_uri'])


class JSONFormat(QueryPlugin):
    """ """
    @staticmethod
    def has_task(task_id):
        """ """
        return task_id in ['json_format']

    @staticmethod
    def perform(task_id, task_name, *args, **kwargs):
        """ """
        with open('/tmp/json_format.log', 'wb') as f:
            f.write('Task: %s\n' % task_id)
            f.write('Args: %s\n' % str(args))
            f.write('Kwargs: %s\n' % str(kwargs))

        if 'json_format' == task_id:
            input_data = args[0]
            output_path = kwargs['outfile']
            result = JSONFormat.json_format(input_data, output_path)
            return True, result, 'Success'
        return False, None, 'Unknown operator %s' % task_id

    @staticmethod
    def expects_uri(task_id):
        """ """
        return False

    @staticmethod
    def is_reducer(task_id):
        """ """
        return False

    @staticmethod
    @operator(
        'formating', 1,
        {'type': dict, 'userDefined': False, 'doc': 'Data to convert to JSON'},
        {'type': str, 'userDefined': True, 'doc': 'Output file'}
    )
    def json_format(data, output_path):
        """ Convert a Python object to JSON.
        ---
        :param data : Data to convert to JSON
        :param output_path : Path of the output file
        """
        json_data = QueryPlugin.dumps(data)
        with open(output_path, 'wb') as f:
            f.write(json_data)
        return json_data
