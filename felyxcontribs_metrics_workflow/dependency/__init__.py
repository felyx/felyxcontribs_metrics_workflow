# encoding: utf-8
"""
dependency
==========
"""
import math
from collections import defaultdict

import numpy

from cerbere.dataset.ncdataset import NCDataset
from felyx_processor.processors.plugins import PluginMetadata, operator
from felyx_processor.processors.plugins.baseplugin import QueryPlugin

info = PluginMetadata('dependency', 'Dependency',
                      ['has_task', 'perform', 'is_reducer', 'expects_uri'])


class Dependency(QueryPlugin):
    @staticmethod
    def has_task(task_id):
        """ """
        return task_id in ['dependency']

    @staticmethod
    def expects_uri(task_id):
        """ """
        return False

    @staticmethod
    def is_reducer(task_id):
        """ """
        return False

    @staticmethod
    def reformat_data(data, target):
        """ """
        for dataset in data:
            for site in data[dataset]:
                if dataset not in target[site]:
                    target[site][dataset] = defaultdict(dict)
                for metric in data[dataset][site]['metrics']:
                    target[site][dataset][metric] = \
                        data[dataset][site]['metrics'][metric]
                target[site][dataset]['time'] = data[dataset][site]['times']

    @staticmethod
    @operator(
        'transformation', 1,
        {'type': NCDataset, 'userDefined': False,
         'doc': 'Input data (result of a selection query)'},
        {'type': list, 'userDefined': True,
         'doc': 'List of X and Y axis configurations'},
        {'type': int, 'userDefined': True,
         'doc': 'Bin size for X parameter'},
        {'type': str, 'userDefined': True,
         'doc': 'Output file (not implemented)'}
    )
    def dependency(input_data=None, cfgs=None, step=0, outfile=None):
        """
        Generate a dependency array from the input data, e.g. the mean and
        standard deviation of Y parameter with respect to bins of X parameter.
        ---
        :param data : Input data (result of a selection query)
        :param cfgs : List of X and Y axis configurations
        :param step : Bin size for X parameter
        :param outfile : Output file (not implemented)
        """

        data, arguments, previous_step = QueryPlugin.get_inputs(
            input_data, 'transformation'
        )

        if cfgs is None:
            cfgs = []

        # Reorganize data
        by_site = defaultdict(dict)

        if isinstance(data, (list, tuple)):
            l = len(data)
            for i in range(0, l):
                Dependency.reformat_data(data[i], by_site)
        else:
            Dependency.reformat_data(data, by_site)

        result = []

        for site in by_site:
            for cfg in cfgs:
                datasets = [cfg[axis].split('/', 1)[0] for axis in cfg]
                datasets = list(set(datasets))
                serie = {'metadata':
                             {'source': '%s - %s' % (','.join(datasets), site)}
                         }
                # keys = cfg.keys()
                axis_data = []
                keys = []
                times = {}
                for axis in cfg:
                    dataset, metric = cfg[axis].split('/', 1)
                    keys.append(axis)
                    axis_data.append(by_site[site][dataset][metric])

                # Filter null values
                axis_data = filter(lambda x: None not in x, zip(*axis_data))
                serie_data = {x[0]: list(x[1:]) if len(x) > 1 else []
                              for x in zip(keys, *axis_data)}
                metric = cfg['y']
                if 0 >= len(serie_data['x']):
                    d = Dependency._format_result(metric, metric, [],
                                                  None, None, None, step)
                else:
                    x_data = numpy.array(serie_data['x'],
                                         dtype=numpy.float)
                    y_data = numpy.array(serie_data['y'],
                                         dtype=numpy.float)
                    v_min = min(x_data)
                    v_max = max(x_data)
                    v_min = step * math.floor(v_min / step) - step / 2.
                    v_max = step * math.ceil(v_max / step) + step / 2.
                    extent = v_max - v_min
                    bins = numpy.linspace(v_min, v_max, extent / step,
                                          endpoint=False)
                    print(bins, v_min, v_max, step, extent)
                    indices = numpy.digitize(x_data, bins)
                    print("###", x_data, bins, indices)
                    y_mean = numpy.ma.masked_all(shape=len(bins))
                    y_std = numpy.ma.masked_all(shape=len(bins))
                    samples = numpy.zeros(shape=len(bins), dtype=numpy.int8)
                    for i in range(len(bins)):
                        print(indices, indices == i, y_data[indices == i+1])
                        samples[i] = len(y_data[indices == i+1])
                        if samples[i] > 0:
                            print(indices[i])
                            y_mean[i] = y_data[indices == i+1].mean()
                            y_std[i] = y_data[indices == i+1].std()
                    print(y_mean, y_std, samples)
                    d = Dependency._format_result(metric,
                                                  metric,
                                                  bins[:] + step / 2.,
                                                  y_mean,
                                                  y_std,
                                                  samples,
                                                  step)
                result.append(d)
                print(result)
            return {
                "results": result,
                "arguments": arguments,
                "step_name": "transformation"
            }

    @staticmethod
    def _format_result(xvarname, yvarname, xvalues,
                       y_mean, y_std, sampling, step):
        """ """
        metadata = {}
        data = {}

        mean_min, mean_max = y_mean.min(), y_mean.max()
        std_min, std_max = y_std.min(), y_std.max()

        y_mean = y_mean.tolist()
        y_std = y_std.tolist()
        xvalues = xvalues.tolist()
        sampling = sampling.tolist()
        print(type(min(xvalues)))

        # X-axis binned values
        # ---------------------------------------------------------------------
        label = 'x'
        metadata[label] = {'min': min(xvalues),
                           'max': max(xvalues),
                           'name': xvarname,
                           'long_name': xvarname,
                           'units': xvarname,
                           'step': step
                           }
        data[label] = {'data': list(xvalues)}

        # Y-axis values
        # ---------------------------------------------------------------------
        label = 'y_mean'
        metadata[label] = {'min': mean_min,
                           'max': mean_max,
                           'name': 'mean',
                           'long_name': '[mean] %s' % yvarname,
                           'units': 'values'
                           }
        data[label] = {'data': y_mean}
        label = 'y_std'
        metadata[label] = {'min': std_min,
                           'max': std_max,
                           'name': 'stddev',
                           'long_name': '[stddev] %s' % yvarname,
                           'units': 'values'
                           }
        data[label] = {'data': y_std}

        # Sampling
        # ---------------------------------------------------------------------
        label = 'y_count'
        metadata[label] = {'min': min(sampling),
                           'max': max(sampling),
                           'name': 'sampling',
                           'long_name': '[sampling] %s' % yvarname,
                           'units': 'values'
                           }
        data[label] = {'data': sampling}

        data['metadata'] = metadata
        return data
