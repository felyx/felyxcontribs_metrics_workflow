# encoding: utf-8
"""
histogram
============
"""

import math
from collections import Counter

import numpy

from cerbere.dataset.ncdataset import NCDataset
from felyx_processor.processors.plugins import PluginMetadata, operator
from felyx_processor.processors.plugins.baseplugin import QueryPlugin

info = PluginMetadata('histogram', 'Histogram',
                      ['has_task', 'perform', 'is_reducer', 'expects_uri'])

class Histogram(QueryPlugin):
    @staticmethod
    def has_task(task_id):
        """ """
        return  task_id in ['histogram1D']  #, 'histogram2D']

    # @staticmethod
    # def perform(task_id, task_name, *args, **kwargs):
    #     """ """
    #     if 'histogram1D' == task_id:
    #         input_data = kwargs.pop('input_data')
    #         from_selection_data = input_data['selection']
    #         series_data = Histogram.histogram1D(from_selection_data, **kwargs)
    #         return True, series_data, 'Success'
    #     elif 'histogram2D' == task_id:
    #         input_data = kwargs.pop('input_data')
    #         series_data = Histogram.histogram2D(input_data['selection'],
    #                                             **kwargs)
    #         return True, series_data, 'Success'
    #     return False, None, 'Unknown operator %s' % task_id

    @staticmethod
    def expects_uri(task_id):
        """ """
        return False

    @staticmethod
    def is_reducer(task_id):
        """ """
        return False

    @staticmethod
    @operator(
        'transformation', 1,
        {'type': NCDataset, 'userDefined': False,
         'doc': 'Input data (result of a selection query)'},
        {'type': int, 'userDefined': True,
         'doc': 'Sampling step'},
        {'type': str, 'userDefined': True,
         'doc': 'Output file (not implemented)'}
    )
    def histogram1D(input_data=None, step=0, outfile=None):
        """
        Generate an histogram from the input data.
        ---
        :param input_data : Input data (result of a selection query
        :param step : Sampling step
        :param outfile : Output file (not implemented)
        """

        data, arguments, previous_step = QueryPlugin.get_inputs(
            input_data, 'transformation'
        )

        result = []
        for dataset in data:
            dataset_data = data[dataset]
            for site in dataset_data:
                source = '%s - %s' % (dataset, site)
                histo = {}
                site_data = dataset_data[site]['metrics']
                for var_name in site_data:
                    valid_data = list(filter(lambda x: x is not None,
                                             site_data[var_name]))
                    if 0 >= len(valid_data):
                        v = [0]
                        c = [0]
                    else:
                        stats = {}
                        num_data = numpy.array(valid_data,
                                               dtype=numpy.float)
                        v_min = min(num_data)
                        v_max = max(num_data)
                        v_min = step * math.floor(v_min / step)
                        v_max = step * math.ceil(v_max / step)
                        extent = v_max - v_min
                        indexation = lambda x: math.floor((x - v_min) / step)
                        indices = map(indexation, num_data)
                        counter = Counter(indices)
                        reverse_indexation = lambda x: v_min + step * x
                        v = map(reverse_indexation, counter.keys())
                        c = counter.values()
                        stats["mean"] = numpy.mean(num_data)
                        stats["stddev"] = numpy.std(num_data)
                    d = Histogram._format_result(source, var_name, v, c, step, stats)
                    result.append(d)

        return {
            "results": result,
            "arguments": arguments,
            "step_name": "transformation"
        }

    @staticmethod
    def _format_result(source, var_name, values, sampling, step, stats):
        """ """
        metadata = {}
        metadata['source'] = source
        metadata['variables'] = [var_name]
        metadata['statistics'] = stats
        data = {}
        # Values
        # ---------------------------------------------------------------------
        label = 'x'
        metadata[label] = {'min': min(values),
                           'max': max(values),
                           'name': var_name,
                           'long_name': var_name,
                           'units': var_name,
                           'step': step
                           }
        data[label] = {'data': values}

        # Sampling
        # ---------------------------------------------------------------------
        label = 'y'
        metadata[label] = {'min': min(sampling),
                           'max': max(sampling),
                           'name': 'sampling',
                           'long_name': '[sampling] %s' % var_name,
                           'units': 'values'
                           }
        data[label] = {'data': sampling}

        data['metadata'] = metadata
        return data

    @classmethod
    def histogram2D(cls, param, param1):
        raise NotImplementedError
