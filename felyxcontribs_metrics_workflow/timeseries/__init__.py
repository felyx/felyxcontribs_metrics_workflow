# encoding: utf-8
"""
timeseries
============
"""

import math
import datetime
import calendar
import traceback

import pytz
import numpy
from cerbere.dataset.ncdataset import NCDataset

from felyx_processor.processors.plugins import PluginMetadata, operator
from felyx_processor.processors.plugins.baseplugin import \
    QueryPlugin, PluginError

info = PluginMetadata(
    'timeseries', 'Timeseries',
    ['has_task', 'perform', 'is_reducer', 'expects_uri'])


class Timeseries(QueryPlugin):
    @staticmethod
    def has_task(task_id):
        """ """
        return task_id in ['timeseries']

    # @staticmethod
    # def perform(task_id, task_name, *args, **kwargs):
    #     """ """
    #     if 'timeseries' == task_id:
    #         input_data = kwargs.pop('input_data')
    #         try:
    #             series_data = Timeseries.timeseries(input_data['selection'],
    #                                                 **kwargs)
    #         except:
    #             print traceback.print_exc()
    #         return True, series_data, 'Success'
    #     return False, None, 'Unknown operator %s' % task_id

    @staticmethod
    def expects_uri(task_id):
        """ """
        return False

    @staticmethod
    def is_reducer(task_id):
        """ """
        return False

    @staticmethod
    def _apply_st_mean(obj):
        """ """
        if numpy.all(numpy.isnan(obj[0])):
            return [float('nan')]
        return numpy.nanmean(obj, axis=0)

    @staticmethod
    def _apply_st_stddev(obj):
        """ """
        if numpy.all(numpy.isnan(obj[0])):
            return [float('nan')]
        return numpy.nanstd(obj, axis=0)

    @staticmethod
    @operator(
        'transformation', 1,
        {'type': NCDataset, 'userDefined': False,
         'doc': 'Input data (result of a selection query)'},
        {'type': str, 'userDefined': True,
         'doc': 'Aggregation mode [year|month|seconds|raw] (default: seconds)'},
        {'type': int, 'userDefined': True,
         'doc': 'Timestep used to regroup values (unit depends on "mode"). '
                'No effect if "mode" is "raw"'},
        {'type': str, 'userDefined': True,
         'doc': 'Start date in YYYY-mm-ddTHH:MM:SS format'},
        {'type': str, 'userDefined': True,
         'doc': 'Stop date in YYYY-mm-ddTHH:MM:SS format'},
        {'type': str, 'userDefined': True,
         'doc': 'Output file (not implemented)'}
    )
    def timeseries(input_data=None, mode=None, step=None, start=None,
                   stop=None, outfile=None):
        """ Generate a timeserie from input data
        ---
        :param input_data: Input data (result of a selection query
        :param mode: Aggregation mode [year|month|seconds|raw]
                     (default: seconds)
        :param step: Timestep used to regroup values (unit depends on "mode").
                     No effect if "mode" is "raw"
        :param start: Start date in YYYY-mm-ddTHH:MM:SS format
        :param stop: Stop date in YYYY-mm-ddTHH:MM:SS format
        :param outfile: Output file (not implemented)
        """

        data, arguments, previous_step = QueryPlugin.get_inputs(
            input_data, 'transformation'
        )

        if mode is None:
            mode = 'raw'
            arguments['transformation']['mode'] = mode

        if step is None:
            step = 0
            arguments['transformation']['step'] = step

        if start is None:
            start = '1970-01-01T00:00:00'
            arguments['transformation']['start'] = start

        if stop is None:
            stop = '2020-12-31T23:59:59'
            arguments['transformation']['stop'] = stop

        var_names = {}
        mean = {}
        stddev = {}
        sampling = {}
        times = {}
        result = []
        # indexation = None
        # reverse_indexation = None

        # Select indexation method
        cfg = Timeseries._select_indexation(mode, start, step)

        for dataset in data:
            dataset_data = data[dataset]
            for site in dataset_data:
                source = '%s - %s' % (dataset, site)
                site_data = dataset_data[site]['metrics']
                _times = dataset_data[site]['times']
                var_names[source] = list(site_data.keys())
                times_indices = map(cfg['indexation'], _times)
                data = list(zip(times_indices, *site_data.values()))

                # Sort raw values
                raw_values = {}
                for i in range(0, len(data)):
                    index = data[i][0]
                    if index in raw_values:
                        raw_values[index].append(numpy.array(data[i][1:],
                                                             dtype=numpy.float))
                    else:
                        raw_values[index] = [numpy.array(data[i][1:],
                                                         dtype=numpy.float)]

                sorted_values = sorted(iter(raw_values.items()),
                                       key=lambda x: x[0])
                sorted_indices = [v[0] for v in sorted_values]
                sorted_values = [v[1] for v in sorted_values]

                try:

                    # Stats
                    _sampling_values = [len(x) for x in sorted_values]
                    _mean_values = map(Timeseries._apply_st_mean,
                                       sorted_values)
                    _stddev_values = map(Timeseries._apply_st_stddev,
                                         sorted_values)
                    _stddev_values = numpy.nan_to_num(_stddev_values)

                    # Map to varnames
                    mean_values = {}
                    stddev_values = {}
                    sampling_values = {}
                    for i in range(0, len(var_names[source])):
                        var_name = var_names[source][i]
                        sampling_values[var_name] = _sampling_values
                        mean_values[var_name] = [v[i] for v in _mean_values]
                        stddev_values[var_name] = [v[i] for v in _stddev_values]

                    # Store results
                    mean[source] = mean_values
                    stddev[source] = stddev_values
                    sampling[source] = sampling_values
                    datetimes = map(cfg['reverse_indexation'], sorted_indices)
                    if 'raw' == mode:
                        times[source] = list(datetimes)
                    else:
                        times[source] = list(
                            map(lambda x: calendar.timegm(x.timetuple()),
                                datetimes)
                        )

                    for i in range(0, len(var_names[source])):
                        r = Timeseries._format_result(
                            source,
                            var_names[source][i],
                            times,
                            mean,
                            stddev,
                            sampling
                        )
                        result.append(r)
                except Exception:
                    # print sorted_values
                    import sys
                    _, e, _ = sys.exc_info()
                    print('error: %s' % str(e))
                    raise PluginError(traceback.print_exc())

        return {
            "results": result,
            "arguments": arguments,
            "step_name": "transformation"
        }

    @staticmethod
    def _format_result(source, var_name, times, mean, stddev, sampling):
        """ """
        metadata = {}
        metadata['source'] = source
        metadata['variables'] = [var_name]

        data = {}
        # Time
        # ---------------------------------------------------------------------
        label = 't'
        values = times[source]
        filtered = list(filter(lambda x: x is not None and not math.isnan(x),
                               values))
        metadata[label] = {'min': min(filtered),
                           'max': max(filtered),
                           'name': 't',
                           'long_name': 'Time',
                           'units': 's'
                           }
        data[label] = {'data': values}

        # Mean value
        # ---------------------------------------------------------------------
        label = '%s_mean' % var_name
        values = mean[source][var_name]
        filtered = list(filter(lambda x: x is not None and not math.isnan(x),
                               values))
        if 0 >= len(filtered):
            min_value = float('nan')
            max_value = float('nan')
        else:
            min_value = min(filtered)
            max_value = max(filtered)
        metadata[label] = {'min': min_value,
                           'max': max_value,
                           'name': var_name,
                           'long_name': '[mean] %s' % var_name,
                           'units': var_name
                           }
        data[label] = {'data': values}

        # Standard deviation
        # ---------------------------------------------------------------------
        label = '%s_stddev' % var_name
        values = stddev[source][var_name]
        filtered = list(filter(lambda x: x is not None and not math.isnan(x),
                               values))
        metadata[label] = {'min': min(filtered),
                           'max': max(filtered),
                           'name': var_name,
                           'long_name': '[stddev] %s' % var_name,
                           'units': var_name
                           }
        data[label] = {'data': values}

        # Sampling
        # ---------------------------------------------------------------------
        label = '%s_sampling' % var_name
        values = sampling[source][var_name]
        filtered = list(filter(lambda x: x is not None and not math.isnan(x),
                               values))
        metadata[label] = {'min': min(filtered),
                           'max': max(filtered),
                           'name': var_name,
                           'long_name': '[sampling] %s' % var_name,
                           'units': 'values'
                           }
        data[label] = {'data': values}

        data['metadata'] = metadata
        return data

    @staticmethod
    def index_by_year(date_str, start, step):
        """ """
        year_str = date_str[:4]
        year_int = int(year_str)
        return math.floor((year_int - start)/step)

    @staticmethod
    def date_from_year(index, start, step):
        year_int = int(start + step * index)
        return datetime.datetime(year_int, 1, 1, 0, 0, 0,
                                 tzinfo=pytz.UTC)

    @staticmethod
    def index_by_month(date_str, start, step):
        year, month = date_str[:7].split('-')
        s_year, s_month = start.split('-')
        m_diff = 12 * (int(year) - int(s_year)) + int(month) - int(s_month)
        return m_diff / step

    @staticmethod
    def date_from_month(index, start, step):
        s_year, s_month = start.split('-')
        m_diff = step * index
        month_int = int(s_month) + m_diff % 12
        year_int = int(s_year) + int(math.floor(m_diff/12))
        return datetime.datetime(year_int, month_int, 1, 0, 0, 0,
                                 tzinfo=pytz.UTC)

    @staticmethod
    def index_by_second(date_str, start, step):
        sec_int = Timeseries._get_timestamp(date_str)
        return math.floor((sec_int - start)/step)

    @staticmethod
    def date_from_second(index, start, step):
        sec_int = start + step * index
        return datetime.datetime.utcfromtimestamp(sec_int)

    @staticmethod
    def _get_timestamp(date_value):
        """ """
        dt = datetime.datetime.strptime(date_value + ' UTC',
                                        '%Y-%m-%d %H:%M:%S %Z')
        return calendar.timegm(dt.timetuple())

    @staticmethod
    def _select_indexation(mode, start, step):
        """ """
        cls = Timeseries
        if 'year' == mode:
            start_year_str = start[:4]
            start = int(start_year_str)
            return {
                'indexation': lambda x: cls.index_by_year(x, start, step),
                'reverse_indexation': lambda x: cls.date_from_year(x,
                                                                   start,
                                                                   step)
            }
        elif 'month' == mode:
            start = start[:7]
            return {
                'indexation': lambda x: cls.index_by_month(x, start, step),
                'reverse_indexation': lambda x: cls.date_from_month(x,
                                                                    start,
                                                                    step)
            }
        elif 'raw' == mode:
            return {
                'indexation': cls._get_timestamp,
                'reverse_indexation': lambda x: x
            }
        else:
            start = cls._get_timestamp(start)
            return {
                'indexation': lambda x: cls.index_by_second(x, start, step),
                'reverse_indexation': lambda x: cls.date_from_second(x,
                                                                     start,
                                                                     step)
            }
