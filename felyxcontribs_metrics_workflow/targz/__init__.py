# -*- coding: utf-8 -*-
from __future__ import with_statement
import os
import tarfile

from felyx_processor.processors.plugins import PluginMetadata, operator

info = PluginMetadata(
    'targz', 'TarGZ',
    ['has_task', 'perform', 'is_reducer', 'expects_uri'])


class TarGZ(object):
    """ Plugin that handles tarball gzipped archives """

    # Operators
    # -------------------------------------------------------------------------
    @staticmethod
    @operator(
        'archiving', 3,
        {'type': list, 'userDefined': False, 'doc': ''},
        {'type': str, 'userDefined': True, 'doc': ''}
    )
    def create_archive(uris, outfile):
        """ Operator that creates a tarball gzipped archive from files located
        in a directory.
        ---
        :param uris : List of paths for the files to include in the archive
        :param outfile : Path of the output file
        """
        print(outfile)
        print(uris)

        cwd = os.getcwd()

        path = uris.common_path()
        if 0 < len(path):
            print('Change dir to %s' % path)
            os.chdir(path)

        f = tarfile.open(outfile, 'w:gz')
        for file in uris:
            print('file = %s' % file)
            fpath = file
            if 0 < len(path):
                fpath = file[len(path):]
            print('fpath = %s' % fpath)

            if os.path.islink(file):
                src = os.readlink(file)
            else:
                src = file
            print('src = %s' % src)
            f.add(name=src, arcname=fpath)
        f.close()

        """
        import subprocess
        print "Stats"
        print "_____"
        for i in xrange(0, len(uris)):
            print "\t%s" % str(os.stat(uris[i]))

        files = ' '.join(map(lambda x: x[len(path):], uris))
        command = 'tar cvf %s %s' % (outfile, files)
        """

        """
        process = subprocess.Popen('tar --version', 
                                   shell=True, 
                                   stdout=subprocess.PIPE, 
                                   stderr=subprocess.PIPE)
        output, errors = process.communicate()
        print 'VERSION :'
        print output
        print errors
        command = 'tar cvf %s %s' % (outfile, ' '.join(uris))
        """

        """
        print 'Execute %s' % command
        process = subprocess.Popen(command, 
                                   shell=True, 
                                   stdout=subprocess.PIPE, 
                                   stderr=subprocess.PIPE)
        output, errors = process.communicate()
        print 'OUTPUT: %s' % output
        print 'ERRORS: %s' % errors
        print 'RETURN: %i' % process.returncode
        os.chdir(cwd)
        """
        """
        os.system(command)
        """
        return outfile

    # Plugin interface implementation
    # -------------------------------------------------------------------------
    @staticmethod
    def has_task(task_id):
        """ """
        return task_id in ['create_archive', 'extract_archive']

    @staticmethod
    def is_reducer(task_id):
        """ """
        return task_id == 'create_archive'

    @staticmethod
    def expects_uri(task_id):
        """ """
        return True

    @staticmethod
    def perform(task_id, task_name, *args, **kwargs):
        """ """
        input_file = args[0]

        if 'create_archive' == task_id:
            output = kwargs['outfile']
            output_file = TarGZ.create_archive(input_file, output)
        elif 'extract_archive' == task_id:
            output_file = None
        else:
            return False, None, '...'
        return True, output_file, 'Success'
