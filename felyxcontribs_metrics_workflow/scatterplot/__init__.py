# encoding: utf-8
"""
scatterplot
============
"""

from collections import defaultdict

from cerbere.dataset.ncdataset import NCDataset

from felyx_processor.processors.plugins import PluginMetadata, operator
from felyx_processor.processors.plugins.baseplugin import QueryPlugin


info = PluginMetadata(
    'scatterplot', 'Scatterplot',
    ['has_task', 'perform', 'is_reducer', 'expects_uri'])


class Scatterplot(QueryPlugin):
    @staticmethod
    def has_task(task_id):
        """ """
        return task_id in ['scatterplot']

    @staticmethod
    def expects_uri(task_id):
        """ """
        return False

    @staticmethod
    def is_reducer(task_id):
        """ """
        return False

    @staticmethod
    def reformat_data(data, target):
        """ """
        for dataset in data:
            for site in data[dataset]:
                if dataset not in target[site]:
                    target[site][dataset] = defaultdict(dict)
                for metric in data[dataset][site]['metrics']:
                    target[site][dataset][metric] = \
                        data[dataset][site]['metrics'][metric]
                target[site][dataset]['time'] = data[dataset][site]['times']

    @staticmethod
    @operator(
        'transformation', 1,
        {'type': NCDataset, 'userDefined': False,
         'doc': 'Input data (result of a selection query)'},
        {'type': list, 'userDefined': True,
         'doc': 'List of scatter configurations'}
    )
    def scatterplot(input_data=None, cfgs=None, **kwargs):
        """
        Generate a scatterplot from the input data.
        ---
        :param data : Input data (result of a selection query)
        :param cfgs : List of scatter configurations
        """
        data, arguments, previous_step = QueryPlugin.get_inputs(
            input_data, 'transformation'
        )

        if cfgs is None:
            cfgs = []

        result = []

        # Reorganize data
        by_site = defaultdict(dict)

        if isinstance(data, (list, tuple)):
            l = len(data)
            for i in range(0, l):
                Scatterplot.reformat_data(data[i], by_site)
        else:
            Scatterplot.reformat_data(data, by_site)

        for site in by_site:
            for cfg in cfgs:
                datasets = [cfg[axis].split('/', 1)[0] for axis in cfg]
                datasets = list(set(datasets))
                serie = {'metadata': {'source': '%s - %s' %
                                                (','.join(datasets), site)}}
                #keys = cfg.keys()
                axis_data = []
                keys = []
                times = {}
                for axis in cfg:
                    dataset, metric = cfg[axis].split('/', 1)
                    keys.append(axis)
                    axis_data.append(by_site[site][dataset][metric])
                    keys.append('%s_times' % axis)
                    axis_data.append(by_site[site][dataset]['time'])

                # Filter null values
                axis_data = filter(lambda x: None not in x, zip(*axis_data))
                serie_data = {x[0]: list(x[1:]) if len(x) > 1 else []
                              for x in zip(keys, *axis_data)}
                for axis in cfg:
                    metric = cfg[axis]
                    serie[axis] = {'data': serie_data[axis]}
                    serie['metadata'][axis] = {
                        'min': min(serie[axis]['data']),
                        'max': max(serie[axis]['data']),
                        'name': metric,
                        'long_name': metric,
                        'units': metric,
                        'times': serie_data['%s_times' % axis]
                    }
                result.append(serie)

            return {
                "results": result,
                "arguments": arguments,
                "step_name": "transformation"
            }
