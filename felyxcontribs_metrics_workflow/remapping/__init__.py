# coding=utf-8
"""
The remapping plugin
--------------------

A plugin to produce remapped miniprods, and return the addresses of those
miniprods in the same format as the input query.

It utilises the QueryPlugin class.

.. :copyright: Copyright 2013 Pelamis Scientific Software Ltd.
.. :license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: David Poulter <david.poulter@pelamis.co.uk>
.. codeauthor:: David Poulter <david.poulter@pelamis.co.uk>
"""
import copy
import datetime
import os
import re

import felyx_processor.storage
import netCDF4
import numpy
import pyproj
from pyresample import geometry, kd_tree
from pyresample.geometry import AreaDefinition
from scipy import stats
from shapely import wkt

from felyx_processor.processors.plugins import PluginMetadata, operator
from felyx_processor.processors.plugins.baseplugin import QueryPlugin, \
    create_derived_path, PluginError
from felyx_processor.query.finders import get_miniprod_path

STANDARD_DATE_UNITS = 'minutes since 1970-01-01 00:00:00'

info = PluginMetadata(
    'remapping', 'Remapping',
    ['has_task', 'perform', 'is_reducer', 'expects_uri']
)


ACCEPTABLE_RESOLUTIONS = numpy.reciprocal((1, 2, 2.5, 4, 5, 10))


def get_file_resolution_and_distance(nc_file):
    try:
        # If the file states its resolution, use it.
        file_resolution = abs(
            min(
                nc_file.getncattr('geospatial_lat_resolution'),
                nc_file.getncattr('geospatial_lon_resolution')
            )
        )
        distance = (2.0 ** 0.5) * 0.5 * 111321 * file_resolution
        return file_resolution, distance

    except (AttributeError, TypeError):
        pass

    try:
        lon = nc_file.variables['lon'][:]
        lat = nc_file.variables['lat'][:]
        eye = numpy.eye(*lon.shape).astype(numpy.bool)
        vector_lon, vector_lat = lon[eye], lat[eye]
        proj = pyproj.Geod(ellps='WGS84')
        distance = numpy.median(
            proj.inv(
                vector_lon,
                vector_lat,
                numpy.roll(vector_lon, 1),
                numpy.roll(vector_lat, 1)
            )[2][1:]
        ) / 2.0
        file_resolution = abs(
            numpy.median(
                lon - numpy.roll(lon, 1)
            )
        )

        return file_resolution, distance
    except Exception:
        pass

    try:
        lon = nc_file.variables['lon'][:]
        file_resolution = abs(numpy.median(lon - numpy.roll(lon, 1)))
        return file_resolution, (2.0 ** 0.5) * 0.5 * 111321 * file_resolution
    except Exception:
        return 5000, 5000


def get_resolution_from_file(nc_file, raw_file_resolution=False):
    """Get a standard form resolution for the input resolution
    which is the smallest of the input resolution values.

    :param str nc_file: The path to a miniprod file.
    :param bool raw_file_resolution: Return the estimated radius_of_influence
      for the regridding.
    """
    if isinstance(nc_file, str):
        with netCDF4.Dataset(nc_file) as nc_file:
            file_resolution, distance = get_file_resolution_and_distance(nc_file)

    elif isinstance(nc_file, netCDF4.Dataset):
        file_resolution, distance = get_file_resolution_and_distance(nc_file)

    else:
        # Other data structures to be added
        raise NotImplementedError(
            'Only opened netCDF files, or paths to netDCF4 files, can be used.'
        )

    # Establish the scale of the resolution
    param = numpy.floor(numpy.log10(file_resolution))
    fixed_resolution = file_resolution * (10 ** (0 - param))

    # Find the next smallest, scale independent, resolution
    inverse_resolution = 1.0 / fixed_resolution
    first_match = numpy.argmin(ACCEPTABLE_RESOLUTIONS > inverse_resolution)
    matched_resolution = ACCEPTABLE_RESOLUTIONS[first_match - 1]

    # Apply known scale and return.
    result_resolution = (1.0 / matched_resolution) * 10.0 ** param
    return result_resolution, distance


def get_output_grid(required_resolution, miniprod_site_bounds):
    """Return a pyresample GridDefinition object for the miniprod site, with
    a set required_resolution.

    :param float required_resolution: The required_resolution of the output
      grid.
    :param tuple miniprod_site_bounds: The boundary of the miniprod site.
    :return: A GridDefinition object.
    """
    xs = numpy.arange(
        miniprod_site_bounds[0], miniprod_site_bounds[2], required_resolution
    ) + required_resolution / 2.0
    ys = numpy.arange(
        miniprod_site_bounds[1], miniprod_site_bounds[3], required_resolution
    ) + required_resolution / 2.0

    lons, lats = numpy.meshgrid(xs, ys)
    out_grid = geometry.GridDefinition(lons=lons, lats=lats)
    return out_grid


def get_miniprod_grid(nc_file):
    """Return a pyresample GridDefinition or SwathDefinition for the
    a miniprod.

    :param str nc_file: The path to a miniprod.
    :return: A GridDefinition or SwathDefinition for the miniprod.
    :raise NotImplementedError: If cdm_feature_type of the miniprod cannot
      be used.
    """
    # Technically unrequired, but improves code automatic code tracking.
    feature_type = None

    if isinstance(nc_file, str):
        with netCDF4.Dataset(nc_file) as nc_file:
            lon = nc_file.variables['lon'][:]
            lat = nc_file.variables['lat'][:]
            if hasattr(nc_file, 'cdm_feature_type'):
                feature_type = nc_file.getncattr('cdm_feature_type')
            elif hasattr(nc_file, 'cdm_data_type'):
                feature_type = nc_file.getncattr('cdm_data_type')
            else:
                raise NotImplementedError(
                    "Miniprods must contain either a cdm_data_type or a "
                    "cdm_feature_type attribute."
                )

            # elif isinstance(nc_file, URL):
            # do the same with contents

    elif isinstance(nc_file, netCDF4.Dataset):
        lon = nc_file.variables['lon'][:]
        lat = nc_file.variables['lat'][:]
        if hasattr(nc_file, 'cdm_feature_type'):
            feature_type = nc_file.getncattr('cdm_feature_type')
        elif hasattr(nc_file, 'cdm_data_type'):
            feature_type = nc_file.getncattr('cdm_data_type')
        else:
            raise NotImplementedError(
                "Miniprods must contain either a cdm_data_type or a "
                "cdm_feature_type attribute."
            )

    else:
        raise NotImplementedError

    if feature_type.lower() == 'grid':
        lons, lats = numpy.meshgrid(lon, lat)
        miniprod_grid = geometry.GridDefinition(lons=lons, lats=lats)
    elif feature_type.lower() == 'swath':
        miniprod_grid = geometry.SwathDefinition(lons=lon, lats=lat)
    else:
        raise NotImplementedError(
            "Only 'grid' or 'swath' features may be for remapped."
        )

    return miniprod_grid


def get_miniprod_bounds(nc_file):
    """Return the bounds of a miniprod site shape

    :param str nc_file: The path to a miniprod.
    :return: The bounds of the miniprod.
    :raise NotImplementedError:
    """
    if isinstance(nc_file, str):
        with netCDF4.Dataset(nc_file) as nc_file:
            site_wkt = nc_file.getncattr('felyx_site_geometry')

            # elif isinstance(nc_file, URL):
            # do the same with contents

    elif isinstance(nc_file, netCDF4.Dataset):
        site_wkt = nc_file.getncattr('felyx_site_geometry')

    else:
        raise NotImplementedError

    shape = wkt.loads(site_wkt)

    return shape.bounds


def get_sizes(miniprod_bounds, required_resolution):
    """Return the number of rows and columns needed to store a miniprod site
    at a set required_resolution.

    :param tuple miniprod_bounds: The bounds of a miniprod site.
    :param float required_resolution: The required_resolution required
    :return:
    """
    x_size = int(
        abs(
            (miniprod_bounds[2] - miniprod_bounds[0]) / required_resolution
        )
    )
    y_size = int(
        abs(
            (miniprod_bounds[3] - miniprod_bounds[1]) / required_resolution
        )
    )
    return x_size, y_size


def get_projection(projection_string, miniprod_bounds, required_resolution):
    """Return a pyresample AreaGeometry object for a miniprod site, at a given
    required_resolution, for a given proj4 projection.

    :param str projection_string: A proj4 string.
    :param tuple miniprod_bounds: The bounds of the miniprod site.
    :param float required_resolution: The required_resolution required for
      the output grid.
    :return: An AreaDefinition object for the required projection.
    """
    # Get corner points
    p = pyproj.Proj(projection_string)
    ll = p(miniprod_bounds[0], miniprod_bounds[1])
    lr = p(miniprod_bounds[2], miniprod_bounds[1])
    ul = p(miniprod_bounds[0], miniprod_bounds[3])
    ur = p(miniprod_bounds[2], miniprod_bounds[3])

    # Establish the area_extent and x / y sizes.
    min_x = min(ll[0], ul[0])
    min_y = min(ll[1], lr[1])
    max_x = max(lr[0], ur[0])
    max_y = max(ul[1], ur[1])
    area_extent = (min_x, min_y, max_x, max_y)
    x_size, y_size = get_sizes(miniprod_bounds, required_resolution)

    # noinspection PyTypeChecker
    # Construct a proj4 argument dictionary from the proj4 string.
    proj4_args = dict(
        _[1:].split('=') for _ in re.findall(r"\+\S*=\S*\w?", p.srs)
    )

    return geometry.AreaDefinition(
        'fake_id', 'fake_name', 'fake_id', proj4_args, x_size, y_size,
        area_extent
    )


def get_indexes(
        file_reference, miniprod_bounds, required_resolution=None,
        projection_string=None, num_neighbours=1):
    """Get the nearest neighbour indexes for miniprod, for a output grid
    definition.

    :param int num_neighbours: The number of neighbours to calculate
    :param str file_reference: The path to the miniprod.
    :param miniprod_bounds: The bounds of the miniprod site.
    :param float required_resolution: The required required_resolution of the
      output grid.
    :param projection_string: A proj4 string defining the required output
      projection.
    :return: The valid input indexes, valid output indexes, index values,
      distances, output grid object and confirmed required_resolution.
    """

    # Establish the required_resolution if needed.

    tmp_required_resolution, distance = get_resolution_from_file(file_reference)
    if required_resolution is None:
        required_resolution = tmp_required_resolution

    # Define the output grid.
    if projection_string is None:
        resultant_grid = get_output_grid(required_resolution, miniprod_bounds)
    else:
        resultant_grid = get_projection(
            projection_string, miniprod_bounds, required_resolution
        )

    # Establish the input grid
    miniprod_grid = get_miniprod_grid(file_reference)

    # Establish the nearest num_neighbours
    input_index, output_index, indexes_array, distances_array = \
        kd_tree.get_neighbour_info(
            miniprod_grid, resultant_grid, distance, neighbours=num_neighbours)
    return input_index, output_index, indexes_array, \
        distances_array, resultant_grid, required_resolution


def is_cylindrical(lons, lats):
    """Return True if the grid is on a cylindrical projection.

    :param lons: Longitude values of the grid.
    :param lats: Latitude values of the grid.
    :return: True if projection is cylindrical else False.
    """
    if len(set(tuple(row) for row in lons)) > 1:
        return True

    if len(set(tuple(row) for row in lats.T)) > 1:
        return True

    return False


def translate_dimensions(data):
    """In the event a Grid miniprod is projected to a non-cylindrical
    projection, the reprojected miniprod's dimension names should be changed.
    This function performs that.

    :param data: The input dimensions.
    :return: The translated dimension names.
    """
    dimension_translation = {'lat': 'nj', 'lon': 'ni'}
    new_dims = []
    for item in data:
        if item in dimension_translation:
            new_dims.append(dimension_translation[item])
        else:
            new_dims.append(item)
    return tuple(new_dims)


def regrid_miniprod(
        input_file, result_file, input_index, output_index,
        indexes, distances, result_grid,
        weight_function=None):
    """Remap the data in input_file to the result_grid, and store in
    result_file.

    :param weight_function: Distance weight function to use in the event of
      more than one neighbour being selected.
    :param str input_file: The path to the input miniprod.
    :param str result_file: The ath for the output remapped miniprod.
    :param numpy.ndarray input_index: An array of valid input indexes.
    :param numpy.ndarray output_index: An array of valid output indexes.
    :param numpy.ndarray indexes: An array of neighbour indexes.
    :param numpy.ndarray distances: An array of neighbour distances.
    :param result_grid: The output grid definition.
    :return: :raise NotImplementedError:
    """
    # Get the mapping type type.
    if weight_function is None:
        grid_type = 'nn'
    else:
        grid_type = 'custom'

    with netCDF4.Dataset(input_file) as nc_file:
        # noinspection PyAugmentAssignment
        with netCDF4.Dataset(result_file, 'w') as out_nc:

            # A container for output dimensions.
            out_dimensions = {}

            # Get the output lat and lon values.
            if isinstance(result_grid, AreaDefinition):
                out_lons, out_lats = result_grid.get_lonlats()
            else:
                out_lons, out_lats = result_grid.lons, result_grid.lats

            # Establish the type of output projection.
            swath = is_cylindrical(out_lons, out_lats)

            for dim_name, dim in nc_file.dimensions.items():

                # Get information for defining output dimensions. Taking
                # into account the cardinality as well as the size may have
                # changed. Force dimension name chagnes if required.
                if dim_name == 'lon':
                    dim_length = out_lons.shape[1]
                    if swath:
                        dim_name = 'ni'
                elif dim_name == 'cell':
                    dim_length = out_lons.shape[1]
                elif dim_name == 'lat':
                    dim_length = out_lats.shape[0]
                    if swath:
                        dim_name = 'nj'
                elif dim_name == 'row':
                    dim_length = out_lats.shape[0]
                else:
                    dim_length = len(dim)

                # Create the required dimension.
                out_dimensions[dim_name] = out_nc.createDimension(
                    dim_name, dim_length
                )

            # A container for the output fields.
            out_variables = {}
            for var_name, var in nc_file.variables.items():

                # Translate dimension names if required.
                if swath:
                    coord_dimensions = translate_dimensions(var.dimensions)
                else:
                    coord_dimensions = var.dimensions

                # Get field attribute values
                data_type = var.dtype

                if hasattr(var, '_FillValue'):
                    # noinspection PyProtectedMember
                    fill_value = var._FillValue
                else:
                    fill_value = None

                # Treat latitude and longitude fields differently
                if var_name in ['lat', 'lon']:

                    # Force dimension name changes if required.
                    if swath:
                        if len(coord_dimensions) == 1:
                            coord_dimensions = ('nj', 'ni')

                    # Construct output variable.
                    out_variables[var_name] = out_nc.createVariable(
                        var_name, data_type, coord_dimensions,
                        fill_value=fill_value, zlib=True, shuffle=True
                    )

                    # Fill values, taking into account dimensionality changes.
                    if var_name == 'lat':
                        if not swath and coord_dimensions != (u'row', u'cell'):
                            out_nc.variables[var_name][:] = out_lats[:, 0]
                        else:
                            out_nc.variables[var_name][:] = out_lats
                    elif var_name == 'lon':
                        if not swath and coord_dimensions != (u'row', u'cell'):
                            out_nc.variables[var_name][:] = out_lons[0, :]
                        else:
                            out_nc.variables[var_name][:] = out_lons
                    else:
                        pass

                # All other variables.
                else:
                    out_variables[var_name] = out_nc.createVariable(
                        var_name, data_type, coord_dimensions,
                        fill_value=fill_value, zlib=True, shuffle=True
                    )

                    if out_variables[var_name].size == indexes.shape[0]:
                        # Extract field data
                        data = var[:]

                        # Remap field data.
                        data_values = kd_tree.get_sample_from_neighbour_info(
                            grid_type, result_grid.shape, data,
                            input_index, output_index,
                            indexes, weight_funcs=weight_function,
                            distance_array=distances,
                            fill_value=None
                        )

                        # Record new field data.
                        out_nc.variables[var_name][:] = data_values

                    elif var_name == 'time':
                        out_nc.variables[var_name][:] = var[:]

                # Write all attributes accept _FillValue (which has already
                # been set.
                for var_attr_name in var.ncattrs():
                    if var_attr_name == '_FillValue':
                        continue
                    setattr(
                        out_nc.variables[var_name], var_attr_name,
                        getattr(var, var_attr_name)
                    )

                # Add coordinate attribute if it didn't already exist.
                if swath:
                    if var_name not in ['lat', 'lon']:
                        print(coord_dimensions)
                        if coord_dimensions[-2:] == ('nj', 'ni'):
                            setattr(
                                out_nc.variables[var_name],
                                'coordinates',
                                'lon lat'
                            )

            # Sync the file
            out_nc.sync()

            # Add global attributes.
            for var_attr_name in nc_file.ncattrs():
                if var_attr_name == '_FillValue':
                    continue
                setattr(
                    out_nc, var_attr_name, getattr(nc_file, var_attr_name)
                )

            # Reset applicable global attributes.
            now = datetime.datetime.now().strftime('%Y%m%dT%H%M%SZ')
            out_nc.date_modified = now
            # noinspection PyAugmentAssignment
            out_nc.title = 'Reprojected ' + out_nc.title
            out_nc.history = 'Fields reprojected by felyx on {}; {}'.format(
                now[:-1], out_nc.history)
            out_nc.geospatial_lon_min = out_lons.min()
            out_nc.geospatial_lon_max = out_lons.max()
            out_nc.geospatial_lat_min = out_lats.min()
            out_nc.geospatial_lat_min = out_lats.max()

    return result_file


def get_weight_function(
        linear_zero_weight_distance=None,
        gaussian_sigma_weight_distance=None):
    """Return a weighting function based one a linear or gaussian distance.
    If gaussian distance is given, the linear distance is ignored.

    :param linear_zero_weight_distance:
    :param gaussian_sigma_weight_distance:
    :return: A weighting function.
    :raise ValueError: If no distances are given.
    """
    if linear_zero_weight_distance is None and \
            gaussian_sigma_weight_distance is None:
        raise ValueError('Either linear_distance or '
                         'gaussian_distance must be given.')

    # Construct a weighting function that returns a zero weight at
    # linear_distance, unity weight at 0 and linear gradations
    # in between
    if linear_zero_weight_distance is not None:
        def linear_weight(distance):
            """Factory generated function. See get_weight_function.

            :param distance: The distances to weigh.
            :return:
            """
            weight = 1.0 - distance / float(linear_zero_weight_distance)
            return weight

        return linear_weight

    # Construct a weighting function that returns a 1-cdf(sigma) weight at
    # gaussian_distance, unity weight at 0 and normal gradations
    # in between. For example:
    #
    # Distance | Weight
    # 0     |   1
    #  sigma   | 0.35
    # 2 sigma  | 0.05
    # 3 sigma  | 0.003

    if gaussian_sigma_weight_distance is not None:
        distribution = stats.norm(0, 1)

        def gaussian_weight(distance):
            """Factory generated function. See get_weight_function

            :param distance: The distances to weigh
            :return:
            """
            return distribution.pdf(
                distance / float(gaussian_sigma_weight_distance)
            )

        return gaussian_weight


def get_sites(input_data, key=None):
    """Return the complete set of unique sites for the query. Note this
    is a nested function.

    :param input_data: The result of a selection or colocation query.
    :return: The set of unique sites in the results of the query so far.
    :raise NotImplementedError:
    """
    site_set = set()
    if isinstance(input_data, list):
        for selection_group in input_data:
            site_set += get_sites(selection_group)
    elif isinstance(input_data, dict):
        if key in input_data:
            working_data = input_data[key]
            if isinstance(input_data[key], dict):
                working_data = (working_data,)
            for selection_group in working_data:
                for site_group in selection_group.values():
                    site_set |= set(site_group.keys())

    return site_set


def get_resolutions(input_data, key=None):
    """Return a dictionary of the most applicable resolution for each site
    in the input query.

    :param input_data: The result of a selection or colocation query.
    :return: A dictionary of resolution for each site.
    :raise NotImplementedError:
    """
    site_resolutions = {}
    if isinstance(input_data, list):
        for selection_group in input_data:
            site_resolutions.update(get_resolutions(selection_group))
    elif isinstance(input_data, dict):
        if key in input_data:
            working_group = input_data[key]
            if isinstance(input_data[key], dict):
                working_group = (working_group,)
            for selection_group in working_group:
                for site_set in selection_group.values():
                    for site, data in site_set.items():
                        if site not in site_resolutions:
                            site_resolutions[site] = get_resolution_from_file(
                                get_miniprod_path(
                                    data['miniprods'][0]
                                )
                            )[0]
                        else:
                            new_resolution = get_resolution_from_file(
                                get_miniprod_path(
                                    data['miniprods'][0]
                                )
                            )[0]
                            if new_resolution < site_resolutions[site]:
                                site_resolutions[site] = new_resolution

    return site_resolutions


class Remapping(QueryPlugin):
    """
    Return the input miniprods reprojected to a new grid.
    """
    requires_task_uuid = True

    @staticmethod
    def has_task(task_id):
        """
        Return True if task_id is 'remapping'.

        :param str task_id: The id of the task to test for.
        :return: True if task_id matches this task.
        :rtype: bool
        """
        return task_id in ['remapping']

    # noinspection PyUnresolvedReferences, PyUnusedLocal
    @staticmethod
    @operator(
        'collocation', 1,
        {'type': dict, 'userDefined': False,
         'doc': 'A selection result'},
        {'type': float, 'userDefined': True,
         'doc': 'The time in minutes for the colocation to be valid.'},
        {'type': str, 'userDefined': True,
         'doc': 'A server:dataset combination.'}
    )
    def remapping(
            input_data, resolution=None, hrdds=False, proj4_string=None,
            linear_zero_weight_distance=None,
            gaussian_sigma_weight_distance=None,
            neighbours=None,
            **kwargs):
        """
        Return the colocations found for the input data. The first provided
        dataset for each site will be treated as the primary dataset, and all
        colocations will be performed with respect to that dataset.

        For example, a colocation of 30 minutes with datasets (A, B, C) will
        mean that the maximum time difference between values of B and C will
        be 60 minutes, but the maximum between A and B OR A and C will be just
        30 minutes.

        The associated_dataset is used to enforce a colocation with
        dynamically produced data, such as in situ data. It takes the form of
        a  "instance:dataset" string. The new instance must be known to this
        felyx instance in order to be used.
        ---
        :param float resolution: The resolution (in decimal degrees) of the
          output data miniprods, if not supplied, the resolution will
          be decided on a per site basis to be the lowest applicable common
          resolution. Ignored if hrdds is set to True.
        :param dict kwargs: Additional keyword arguments to be ignored.
        :param input_data: A selection or olocation result
        :type input_data: dict or list
        :param bool hrdds: Set true to produce output in old GHRSST standard
          HR-DDS projection, a rectangular equidistant grid at 0.01 degree
          resolution covering only the miniprod site. If true, then resolution
          and proj4_string are ignored.
        :param str proj4_string: A proj4 string defining the output projection.
          Ignored if hrdds is set to true.
        :param float linear_zero_weight_distance: The distance in metres at
          which point a neighbour has zero weight. Pixels further apart than
          this distance have no influence on each other in =remapping.
          Setting this value will cause the remapping result of each pixel
          to be the mean value of it's neighbours, weighted linearly by
          distance, with a zero weight at this distance. The number of
          neighbours is given by the neighbours variable. Ignored if
          gaussian_distance is given.
        :param float gaussian_sigma_weight_distance: The distance at which
          point at least 68% (one sigma) of total weights lie within. At least
          95% (2 sigma) of total weights will lie within twice this distance,
          and 99.7 (3 sigma) of total weights will lie within three times
          this distance. These are minimum values, as the distribution of
          weights is limited by the number of neighbours taken into account.
          In effect, a pixel at this distance will have a weight of ~32%, at
          twice this distance a weight of 5% and three times this distance a
          weight of 0.3%. Giving this value will cause
          linear_distance to be ignored.
        :param int neighbours: Ignored if neither linear_distance
          nor gaussian_distance are set (in which case a nearest
          neighbour remapping is performed, and it effectively defaults to 1.
          If a weighted remapping is requested, neighbours defaults to 4.
        :return:
        """
        # Grab previous plugin arguments
        input_data, arguments, previous_step = QueryPlugin.get_inputs(
            input_data, 'remapping'
        )

        if previous_step not in ['selection', 'colocation']:
            raise PluginError(
                "The input to the remmaping plugin must contain "
                "either the results from a selection step or a "
                "colocation plugin."
            )
        else:
            key = previous_step

        result = {key: copy.deepcopy(input_data)}

        # pyproj does not support unicode projection strings
        if proj4_string is not None:
            proj4_string = str(proj4_string)

        # Get the root names for the derived miniprods. The root includes a
        # hash value of the plugins inputs, to avoid regenerating the results.
        remapped_name, remapped_path = create_derived_path(
            'remapped_miniprods',
            resolution=resolution,
            hrdds=hrdds,
            proj4_string=proj4_string,
            linear_zero_weight_distance=linear_zero_weight_distance,
            gaussian_sigma_weight_distance=gaussian_sigma_weight_distance,
            neighbours=neighbours
        )

        if hrdds:
            resolution = 0.01
            neighbours = None
            linear_zero_weight_distance = None
            gaussian_sigma_weight_distance = None
            proj4_string = None

        # Get the neighbours and weighting function
        if linear_zero_weight_distance is not None:
            weight_func = get_weight_function(
                linear_zero_weight_distance=linear_zero_weight_distance
            )
            if neighbours is None:
                neighbours = 4
        elif gaussian_sigma_weight_distance is not None:
            weight_func = get_weight_function(
                gaussian_sigma_weight_distance=gaussian_sigma_weight_distance
            )
            if neighbours is None:
                neighbours = 4
        else:
            weight_func = None
            neighbours = 1

        arguments['remapping']['neighbours'] = neighbours
        arguments['remapping']['proj4_string'] = proj4_string

        # Get the unique sites
        unique_sites = get_sites(result, key)

        # Get dictionary of resolutions
        if resolution is not None:
            arguments['remapping']['resolution'] = resolution
            resolutions = {site: resolution for site in unique_sites}
        else:
            resolutions = get_resolutions(result, key)
            arguments['remapping']['resolution'] = resolutions

        site_bounds = {}

        working_selection = result[key]
        if isinstance(working_selection, dict):
            working_selection = (working_selection,)
        for index, selection in enumerate(working_selection):
            for data_set_name, sites in selection.items():
                for site_name, values in sites.items():
                    if 'miniprods' not in values:
                        result[key][data_set_name][site_name] = {
                            "miniprods": [], "times": []
                        }
                        continue
                    for mp_index, name in enumerate(values['miniprods']):

                        # Get miniprod path
                        path = get_miniprod_path(name)

                        # Get paths for output products, ensure no repeat
                        # generation.
                        # noinspection PyTypeChecker

                        path_date = datetime.datetime.strptime(
                                        name[:8], "%Y%m%d"
                                    )
                        # noinspection PyArgumentList
                        modifier = "/".join(
                            [
                                site_name,
                                data_set_name,
                                path_date.strftime("%Y/%j")
                            ]
                        )

                        output_path = '/'.join(
                            [remapped_path, modifier]
                        )

                        felyx_processor.storage.makedirs(output_path)

                        output_name = '/'.join(
                            [remapped_name, modifier]
                        )

                        out_file = os.path.join(
                            output_path, name
                        )

                        if not os.path.exists(out_file):
                            if site_name not in site_bounds:
                                site_bounds[site_name] = get_miniprod_bounds(
                                    path
                                )

                            input_index, output_index, indexes, distances, \
                                grid, grid_resolution = \
                                get_indexes(
                                    path, site_bounds[site_name],
                                    projection_string=proj4_string,
                                    required_resolution=resolutions[site_name],
                                    num_neighbours=neighbours
                                )

                            # try:
                            new_miniprod = regrid_miniprod(
                                path, out_file, input_index,
                                output_index, indexes, distances,
                                grid, weight_function=weight_func
                            )

                        else:
                            new_miniprod = out_file

                        if new_miniprod:
                            if isinstance(result[key], dict):
                                result[key][data_set_name]\
                                    [site_name]['miniprods'][mp_index] = \
                                    os.path.join(
                                        output_name, name
                                    )
                            else:
                                result['selection'][index][data_set_name]\
                                    [site_name]['miniprods'][mp_index] = \
                                    os.path.join(
                                        output_name, name
                                    )
                        else:
                            if isinstance(result[key], dict):
                                result[key][data_set_name]\
                                    [site_name]['miniprods'][mp_index] =\
                                    None
                            else:
                                result[key][index][data_set_name]\
                                    [site_name]['miniprods'][mp_index] = \
                                    None
        return {
            'results': result.pop(key),
            'arguments': arguments,
            'step_name': 'remapping'
        }

