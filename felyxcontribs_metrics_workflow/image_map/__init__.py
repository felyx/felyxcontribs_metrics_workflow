# coding=utf-8
"""
The image_map plugin
--------------------

A plugin to produce render 2d images from fields, and return the addresses of
those rendered images in the same format as the input query.

It utilises the QueryPlugin class.

.. :copyright: Copyright 2014 Pelamis Scientific Software Ltd.
.. :license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: David Poulter <david.poulter@pelamis.co.uk>
.. codeauthor:: David Poulter <david.poulter@pelamis.co.uk>
"""
import datetime
import itertools
import os

import matplotlib

matplotlib.use('Agg')
from matplotlib.pyplot import figure, savefig, close
import matplotlib.cm as cm

from felyx_processor.processors.plugins import PluginMetadata, operator
from felyx_processor.processors.plugins.baseplugin import QueryPlugin, \
    create_derived_path
from felyx_processor.query.finders import get_miniprod_path, get_miniprod_root
import felyx_processor.storage

import netCDF4
import numpy
import copy

STANDARD_DATE_UNITS = 'minutes since 1970-01-01 00:00:00'

info = PluginMetadata(
    'image_map', 'ImageMap',
    ['has_task', 'perform', 'is_reducer', 'expects_uri']
)



ACCEPTABLE_RESOLUTIONS = numpy.reciprocal((1, 2, 2.5, 4, 5, 10))
MINIPROD_ROOT = get_miniprod_root()


def get_range_from_file(nc_file, field):
    """Get a tuple of min and max values for the file.

    :param str nc_file: The path to a miniprod file.
    :param str field: The field to examine.
    """
    if isinstance(nc_file, str):
        with netCDF4.Dataset(nc_file) as data_file:
            values = data_file.variables[field][:]
            value_range = [float(values.min()), float(values.max())]

    elif isinstance(nc_file, netCDF4.Dataset):
        values = nc_file.variables[field][:]
        value_range = [float(values.min()), float(values.max())]
    else:
        # Other data structures to be added
        raise NotImplementedError(
            'Only opened netCDF files, or paths to netDCF4 files, can be used.'
        )
    return value_range


def get_sites(input_data, key):
    """Return the complete set of unique sites for the query. Note this
    is a nested function.

    :param str key: The source of the input for this plugin, as the name of
      a previous plugin stage. For example, 'selection' or 'colocation'.
    :param input_data: The result of a selection or colocation query.
    :return: The set of unique sites in the results of the query so far.
    :raise NotImplementedError:
    """
    site_set = set()
    if isinstance(input_data, list):
        for selection_group in input_data:
            site_set += get_sites(selection_group, key)
    elif isinstance(input_data, dict):
        if key in input_data:
            working_data = input_data[key]
            if isinstance(input_data[key], dict):
                working_data = (working_data,)
            for selection_group in working_data:
                for site_group in selection_group.values():
                    site_set |= set(site_group.keys())

    return site_set


def get_value_range(input_data, key, field):
    """Return a dictionary of the min and max values for each site.

    :param str field: The name of the field to extract the information from.
    :param str key: The source of the input for this plugin, as the name of
      a previous plugin stage. For example, 'selection' or 'colocation'.
    :param input_data: The result of a selection or colocation query.
    :return: A dictionary of resolution for each site.
    :raise NotImplementedError:
    """
    site_range = {}
    if isinstance(input_data, list):
        for selection_group in input_data:
            site_range.update(get_value_range(selection_group, key, field))
    elif isinstance(input_data, dict):
        if key in input_data:
            working_group = input_data[key]
            if isinstance(input_data[key], dict):
                working_group = (working_group,)
            for selection_group in working_group:
                for site_set in selection_group.values():
                    for site, data in site_set.items():
                        if site not in site_range:
                            site_range[site] = get_range_from_file(
                                get_miniprod_path(
                                    data['miniprods'][0]
                                ),
                                field
                            )
                        else:
                            new_range = get_range_from_file(
                                get_miniprod_path(
                                    data['miniprods'][0]
                                ),
                                field
                            )
                            if new_range < site_range[site]:
                                site_range[site] = new_range

    return site_range


def render(
        miniprod_path, output_file, field, dpi=100, cmap=None,
        value_range=(None, None)):
    """

    :param miniprod_path: The full path to the input miniprod.
    :param output_file: The full path to write the rendered image to.
    :param field: The name of the field to extract.
    :param dpi: The resolution of the image in dots per inch. Defaults to 100.
    :param cmap: The matplotlib colour map to be used, defaults to 'jet'.
    :param value_range: A tuple of (min_value, max_value) for the image to
      render. Above or below these limits the data will be saturated.
    :return:
    """

    with netCDF4.Dataset(miniprod_path) as nc_file:
        data = nc_file.variables[field][:]

    if data.ndim == 3:
        data = data[0, :, :]

    fig = figure(figsize=[float(_) / dpi for _ in data.shape])

    ax = fig.add_axes([0, 0, 1, 1])
    ax.imshow(
        data, origin='lower', interpolation='none', vmin=value_range[0],
        vmax=value_range[1], cmap=cmap
    )

    fig.patch.set_visible(False)
    ax.axis('off')
    savefig(output_file, dpi=dpi)
    close(fig)
    return output_file


class ImageMap(QueryPlugin):
    """
    Return the input miniprods reprojected to a new grid.
    """
    requires_task_uuid = True

    @staticmethod
    def has_task(task_id):
        """
        Return True if task_id is 'remapping'.

        :param str task_id: The id of the task to test for.
        :return: True if task_id matches this task.
        :rtype: bool
        """
        return task_id in ['image_map']

    # # noinspection PyUnresolvedReferences,PyUnusedLocal
    @staticmethod
    @operator(
        'image_map', 1,
        dict(type=dict, userDefined=False, doc='A selection result'),
        dict(
            type=float, userDefined=True,
            doc='The time in minutes for the colocation to be valid.'
        ),
        dict(type=str, userDefined=True, doc='A server:dataset combination.')
    )
    def image_map(
            input_data, value_range=None, field=None, dpi=None,
            image_format=None, colour_map=None, **kwargs):
        """
        ___
        :param input_data: The results form a previous plugin.
        :param tuple value_range: The range of values outside of which the
          image will saturate
        :param str field: The name of the field containing the data.
        :param int dpi: The resolution of the rendered image in dots per inch
        :param str image_format: The format of the image to pe produced, in
          form of the extension. For example 'png' creates a PNG image,
        :param kwargs:
        :return:
        """
        input_data, arguments, previous_step = QueryPlugin.get_inputs(
            input_data, 'formatting'
        )

        if dpi is None:
            dpi = 100
            arguments['formatting']['dpi'] = dpi
        if image_format is None:
            image_format = 'png'
            arguments['formatting']['image_format'] = image_format
        if colour_map is None:
            colour_map = 'jet'
        cmap = cm.get_cmap(colour_map)
        gradient = numpy.linspace(0, 1, 256)
        rgbas = [cmap(i) for i in gradient]
        rgbas = list(map(lambda x: [int(255 * x[0]), int(255 * x[1]),
                                    int(255 * x[2]), x[3]], rgbas))
        arguments['formatting']['rgba_colours'] = rgbas
        arguments['formatting']['colour_map'] = colour_map

        key = previous_step
        input_data = {key: input_data}

        try:
            cmap = cm.get_cmap(name=colour_map)
        except Exception:
            cmap = cm.get_cmap()

        if key in ['selection', 'colocation']:
            rendered_name, rendered_path = create_derived_path(
                'formatted_miniprods',
                value_range=value_range,
                field=field,
                dpi=dpi,
                image_format=image_format,
                cmap=cmap.name
            )

        # Get the unique sites
        unique_sites = get_sites(input_data, key)

        # Get dictionary of resolutions
        if value_range is not None:
            value_ranges = {site: value_range for site in unique_sites}
        else:
            value_ranges = get_value_range(input_data, key, field)
        arguments['formatting']['value_range'] = value_ranges

        result = copy.deepcopy(input_data)

        working_selection = result[key]
        if isinstance(working_selection, dict):
            working_selection = (working_selection,)
        # elif isinstance(working_selection, list):
        #     working_selection = tuple(_ for _ in working_selection)
        for index, selection in enumerate(working_selection):
            for data_set_name, sites in selection.items():
                for site_name, values in sites.items():
                    if 'miniprods' not in values:
                        result[key][data_set_name][site_name] = {
                            "miniprods": [], "times": []
                        }
                        continue
                    if isinstance(result[key], list):
                        result[key] = dict(
                            itertools.chain.from_iterable(
                                _.items() for _ in result[key]
                            )
                        )

                    for mp_index, name in enumerate(values['miniprods']):

                        path = get_miniprod_path(name)

                        if key in ['remapping']:
                            previous_hash = name.split('/')[1]
                            rendered_name, rendered_path = create_derived_path(
                                'formatted_miniprods',
                                value_range=value_range,
                                field=field,
                                dpi=dpi,
                                image_format=image_format,
                                cmap=cmap.name,
                                previous_hash=previous_hash
                            )

                        # Get paths for output products, ensure no repeat
                        # generation.
                        # noinspection PyTypeChecker

                        path_date = datetime.datetime.strptime(
                            name.split('/')[-1][:8], "%Y%m%d"
                        )

                        # noinspection PyArgumentList
                        modifier = "/".join(
                            [
                                site_name,
                                data_set_name,
                                datetime.datetime.strftime(
                                    path_date,
                                    "%Y/%j"
                                )
                            ]
                        )

                        output_path = '/'.join(
                            [rendered_path, modifier]
                        )

                        felyx_processor.storage.makedirs(output_path)

                        output_name = '/'.join(
                            [rendered_name, modifier]
                        )

                        out_file = "{}.{}".format(
                            os.path.join(output_path, os.path.basename(name)),
                            image_format
                        )

                        # try:
                        if not os.path.exists(out_file):
                            new_miniprod = render(
                                path, out_file, field, dpi, cmap,
                                value_range=value_ranges[site_name]
                            )
                        else:
                            new_miniprod = out_file

                        if new_miniprod:
                            if isinstance(result[key], dict):
                                result[key][data_set_name]\
                                    [site_name]['miniprods'][mp_index] = \
                                    os.path.join(
                                        output_name,
                                        os.path.basename(new_miniprod)
                                    )
                            else:
                                result[key][index][data_set_name]\
                                    [site_name]['miniprods'][mp_index] = \
                                    os.path.join(
                                        output_name,
                                        os.path.basename(new_miniprod)
                                    )
                        else:
                            if isinstance(result[key], dict):
                                result[key][data_set_name]\
                                    [site_name]['miniprods'][mp_index] = None
                            else:
                                result[key][index][data_set_name]\
                                    [site_name]['miniprods'][mp_index] = None

        return {
            'results': result.pop(key),
            'arguments': arguments,
            'step_name': 'formatting'
        }
