# encoding: utf-8
"""
The json plugin
------------------

This plugin stores the textual results of a query in a JSON format.
It accepts only the results of a query step (or set of selection steps) and as
such is a query level plugin.

It utilises the QueryPlugin class.

.. :copyright: Copyright 2014 Pelamis Scientific Software Ltd.
.. :license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: David Poulter <david.poulter@pelamis.co.uk>
.. codeauthor:: David Poulter <david.poulter@pelamis.co.uk>
"""
import json

from felyx_processor.processors.plugins import PluginMetadata, operator
from felyx_processor.processors.plugins.baseplugin import QueryPlugin
# Added to fix issue with module import name in testing. The following line
# can be removed from production code
from felyx_processor.query.handler import add_urls

dumps = json.dumps

info = PluginMetadata(
    'json', 'JSON',
    ['has_task', 'perform', 'is_reducer', 'expects_uri']
)


def clean(value):
    """Strip any parameter named 'miniprod' from the input.

    :param value: The input to clean.
    """

    if isinstance(value, dict):
        if 'miniprods' in value.keys():
            del value['miniprods']
        for _ in value.values():
            clean(_)
    elif isinstance(value, list):
        for _ in value:
            clean(_)
    else:
        pass


class JSON(QueryPlugin):
    """Produce a JSON file storing the results from a query."""

    @staticmethod
    def has_task(task_id):
        """
        Return True if task_id is 'json'.

        :param str task_id: The id of the task to test for.
        :return: True if task_id matches this task.
        :rtype: bool
        """
        return task_id in ['json']

    # noinspection PyUnusedLocal
    @staticmethod
    @operator(
        'packaging', 1, {
            'type': dict, 'userDefined': False,
            'doc': 'A dictionary of input values'
        }, {
            'type': str, 'userDefined': True,
            'doc': 'The path of the output file'
        },
        {
            'type': bool, 'userDefined': True,
            'doc': 'Include the internal felyx representation miniprods as '
                   'as well as the URI to those miniprods.'
        },
    )
    def json(input_data=None, outfile='', include_local_names=False):
        """
        Stores the output from a query in JSON format.
        ---
        :param input_data: A dictionary of input values.
        :type input_data: dict or list
        :param str outfile: The path of the output file.
        :param bool include_local_names: Include the internal felyx
          representation miniprods as as well as the URI to those miniprods.
        """
        values, arguments, previous_step = QueryPlugin.get_inputs(
            input_data, 'packaging'
        )
        values = add_urls(values)

        if not include_local_names:
            arguments['packaging']['include_local_names'] = include_local_names
            clean(values)

        results = {
            'results': values,
            'arguments': arguments,
        }

        json_data = QueryPlugin.dumps(results)

        with open(outfile, 'wb') as output_file:
            output_file.write(json_data)

        return True
