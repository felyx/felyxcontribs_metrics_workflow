# coding=utf-8
"""
The colocation plugin
---------------------

A simple colocation engine for metrics and miniprods. It accepts only the
results of a selection step (or set of selection steps) and as such is a
query level plugin.

It utilises the QueryPlugin class.

.. :copyright: Copyright 2013 Pelamis Scientific Software Ltd.
.. :license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: David Poulter <david.poulter@pelamis.co.uk>
.. codeauthor:: David Poulter <david.poulter@pelamis.co.uk>
"""

import datetime
import itertools
import json
from urllib.parse import urljoin

import numpy
import requests
from netCDF4 import date2num, num2date
from numpy import isfinite
from scipy.spatial.ckdtree import cKDTree

from felyx_processor.processors.plugins import PluginMetadata, operator
from felyx_processor.processors.plugins.baseplugin import QueryPlugin, \
    PluginError
from felyx_processor.utils.configuration import get_felyx_config

STANDARD_DATE_UNITS = 'minutes since 1970-01-01 00:00:00'

info = PluginMetadata(
    'colocation', 'Colocation',
    ['has_task', 'perform', 'is_reducer', 'expects_uri']
)


CONFIG = get_felyx_config()
INSTANCE = CONFIG.get('external_instances', {})


def d2n(date):
    """Helper function for date to number conversion.

    :param str date: A timestamp string.

    :return: The data as seconds from STANDARD_DATA_UNITS.
    :rtype: float
    """
    return date2num(
        datetime.datetime.strptime(
            date, '%Y-%m-%dT%H:%M:%S'
        ), STANDARD_DATE_UNITS
    )


def n2d(number):
    """Helper function for number to date conversion.

    :param float number: Seconds since STANDARD_DATE_UNITS
    :return: number as a datetime
    :rtype: :class:`datetime.datetime`
    """
    return num2date(
        number, STANDARD_DATE_UNITS
    )


class SiteSelection(object):
    """
    A class representing a selection from felyx for a particular site. This
    object is used to store the KDTree (and other data) that is used to perform
    a per-site colocation.
    """

    def __init__(self):
        self.__other_datasets = {}
        self.__primary_dataset_name = None
        self.__primary_dataset_data = None
        self.__primary_time_array = None
        self.__primary_tree = None
        self.other_trees = {}

    @property
    def primary_dataset(self):
        """
        A tuple of the primary dataset name and its selection data.
        """
        return self.__primary_dataset_name, self.__primary_dataset_data

    @property
    def additional_datasets(self):
        """
        Generator for the additional datasets.

        .. note:: Internally, this is a generator. This saves much memory, but
            restricts execution to one-shot.
        """
        for dataset_name, dataset_data in self.__other_datasets.items():
            yield dataset_name, dataset_data

    @property
    def primary_time_array(self):
        """
        A numerical array for the primary dataset times.
        """
        if self.__primary_time_array is None:
            self.__primary_time_array = numpy.array(
                [(d2n(_), 0) for _ in self.primary_dataset[1]['times']]
            )

        return self.__primary_time_array

    @property
    def primary_tree(self):
        """
        The KDTree for the times of the primary dataset.
        """
        if self.__primary_dataset_name is None:
            return None

        if self.__primary_tree is None:
            self.__primary_tree = cKDTree(self.primary_time_array)
        return self.__primary_tree

    @classmethod
    def get_tree(cls, times):
        """
        Return a cKDTree object for a set of times.

        :param list times: The times to be incorporated into a Tree
        :return: A cKDTree object.
        """
        return cKDTree(
            numpy.array(
                [(d2n(_), 0) for _ in times]
            )
        )

    def add_dataset(self, dataset_name, dataset_data):
        """
        Add a dataset selection result to this site selection. Store the
        result in the object.

        :param str dataset_name: The name of the dataset being added.
        :param dict dataset_data: The selection data for the dataset
          being added.
        :return: None
        """
        if self.__primary_dataset_name is None:
            self.__primary_dataset_name = dataset_name
            self.__primary_dataset_data = dataset_data
        else:
            self.__other_datasets[dataset_name] = dataset_data

    def calculate_matches(self, colocation_window=30):
        """
        Calculate the matchups between the the primary selection and the other
        selection components in the selection. Store the result internally.

        :param int colocation_window: The maximum separation in minutes.
        :return: None
        """
        for name, selection_data in self.__other_datasets.items():
            tree = SiteSelection.get_tree(selection_data['times'])
            distance, index = tree.query(
                self.primary_time_array, distance_upper_bound=colocation_window
            )
            mask = isfinite(distance)
            indexes = index * mask
            self.other_trees[name] = {'mask': mask, 'indexes': indexes}

    def get_mask(self):
        """
        Return the combined mask for all these datasets.
        """
        if self.other_trees == {}:
            return None

        # noinspection PyTypeChecker
        return numpy.prod(
            [_['mask'] for _ in self.other_trees.values()],
            axis=0
        ).astype(numpy.bool)


class Selections(object):
    """A class representing the results of a selection query step. This may
    include multiple sites, datasets or indeed multiple selection statements.
    """

    def __init__(self):
        self.__sites = {}

    def __iter__(self):
        """Ensure that this object is an iterator, and iterate around the
        values in self.__sites. These are the sites added by the
        "add_selection" method.
        """
        for key, value in self.__sites.items():
            yield key, value

    def add_selection(self, site, dataset_name, dataset_data):
        """Add the selection results for a dataset / site combination to this
        object and store internally.

        :param str site: The name of the site being added.
        :param str dataset_name: The name of the dataset being added.
        :param dict dataset_data: The selection data for the
          dataset being added.
        :return: None
        """
        if site not in self.__sites.keys():
            self.__sites[site] = SiteSelection()

        self.__sites[site].add_dataset(dataset_name, dataset_data)


class Colocation(QueryPlugin):
    """
    Return a list of matched pairs of metrics or miniprods.
    """

    @staticmethod
    def has_task(task_id):
        """
        Return True if task_id is 'colocation'.

        :param str task_id: The id of the task to test for.
        :return: True if task_id matches this task.
        :rtype: bool
        """
        return task_id in ['colocation']

    # noinspection PyUnresolvedReferences,PyUnusedLocal
    @staticmethod
    @operator(
        'collocation', 1,
        {'type': dict, 'userDefined': False,
         'doc': 'A selection result'},
        {'type': float, 'userDefined': True,
         'doc': 'The time in minutes for the colocation to be valid.'},
        {'type': str, 'userDefined': True,
         'doc': 'A server:dataset combination.'}
    )
    def colocation(
            input_data, maximum_time_difference=None,
            associated_dataset=None, associated_fields=None, **kwargs):
        """
        Return the colocations found for the input data. The first provided
        dataset for each site will be treated as the primary dataset, and all
        colocations will be performed with respect to that dataset.

        For example, a colocation of 30 minutes with datasets (A, B, C) will
        mean that the maximum time difference between values of B and C will
        be 60 minutes, but the maximum between A and B OR A and C will be just
        30 minutes.

        The associated_dataset is used to enforce a colocation with
        dynamically produced data, such as in situ data. It takes the form of
        a  "instance:dataset" string. The new instance must be known to this
        felyx instance in order to be used.
        ---
        :param str associated_dataset: A server:dataset combination
        :param dict kwargs: Additional keyword arguments to be ignored.
        :param input_data: A selection result
        :type input_data: dict or list
        :param int maximum_time_difference: The time in minutes for the
          colocation to be valid.
        :return:
        """
        return_value = {}

        input_data, arguments, previous_step = QueryPlugin.get_inputs(
            input_data, 'colocation'
        )

        if maximum_time_difference is None:
            maximum_time_difference = 30
            arguments['colocation']['maximum_time_difference'] = \
                maximum_time_difference

        input_data_type = None

        if associated_dataset:
            associated_server_name, associated_dataset_name = \
                associated_dataset.split(':')
            config = get_felyx_config()
            instance = config.get('external_instances', {})
        else:
            associated_dataset_name, associated_server_name, \
                instance = None, None, None

        if input_data is None:
            raise PluginError(
                "No valid data was received into the colocation step of your "
                "query from the selection step(s) you specified. Please test "
                "the selection steps you requested, as no olocation can be "
                "performed on them as they are."
            )
        else:
            # There are is a valid selection.
            selections = Selections()
            result = {}

            if isinstance(input_data, dict):
                # This is a matchup between two datasets only
                for dataset_name, site_group in input_data.items():
                    for site_name, data_group in site_group.items():
                        selections.add_selection(
                            site_name, dataset_name, data_group
                        )

            else:
                # This is a multi selection matchup
                for selection_id, selection in enumerate(input_data):
                    for dataset_name, site_group in selection.items():
                        for site_name, data_group in site_group.items():
                            selections.add_selection(
                                site_name, dataset_name, data_group
                            )

            for site, site_data in selections:
                site_data.calculate_matches(maximum_time_difference)
                mask = site_data.get_mask()
                if mask is None:
                    continue
                primary_name, primary_data = site_data.primary_dataset
                if primary_name not in result.keys():
                    result[primary_name] = {}

                    if associated_dataset:
                        return_value["primary_dataset"] = primary_name
                        return_value["associated_dataset"] = \
                            associated_dataset_name
                    if associated_fields:
                        return_value["associated_fields"] = associated_fields

                if site not in result[primary_name].keys():
                    result[primary_name][site] = {}

                if 'miniprods' in primary_data:
                    result[primary_name][site]['miniprods'] = list(
                        itertools.compress(primary_data['miniprods'], mask)
                    )
                    input_data_type = 'miniprods'

                if 'metrics' in primary_data:
                    result[primary_name][site]['metrics'] = {}
                    for metric in primary_data['metrics'].keys():
                        result[primary_name][site]['metrics'][metric] = list(
                            itertools.compress(
                                primary_data['metrics'][metric], mask
                            )
                        )
                    input_data_type = 'metrics'

                result[primary_name][site]['times'] = list(
                    itertools.compress(primary_data['times'], mask)
                )

                for dataset_name, dataset_data in site_data.additional_datasets:
                    if dataset_name not in result.keys():
                        result[dataset_name] = {}

                    if site not in result[dataset_name].keys():
                        result[dataset_name][site] = {}

                    indexes = (
                        numpy.array(
                            site_data.other_trees[dataset_name]['indexes'][mask]
                        )[site_data.other_trees[dataset_name]['mask'][mask]]
                    ).tolist()

                    result[dataset_name][site]['times'] = [
                        dataset_data['times'][_] for _ in indexes
                    ]

                    if 'miniprods' in dataset_data:
                        result[dataset_name][site]['miniprods'] = [
                            dataset_data['miniprods'][_] for _ in indexes
                        ]

                    if 'metrics' in dataset_data:
                        result[dataset_name][site]['metrics'] = {}
                        for metric in dataset_data['metrics'].keys():
                            result[dataset_name][site]['metrics'][metric] = \
                                [dataset_data['metrics'][metric][_]
                                for _ in indexes
                            ]

        if result == {}:
            raise PluginError("This colocation produced no matchups.")

        if associated_server_name is not None:

            if associated_server_name not in instance.keys():
                raise PluginError(
                    str(
                        "The in situ instance {} is not known to this "
                        "felyx instance."
                    ).format(associated_server_name)
                )

            try:

                if input_data_type == 'metrics' and associated_fields is None:
                    raise PluginError(
                        "If you make a metric colocation with associated ("
                        "in situ) data, you must provide the 'associated_"
                        "fields' list object in the query."
                    )

                request_url = request_url = urljoin(
                    instance[associated_server_name], 'extraction/extraction/'
                )

                return_value["colocation"] = result
                return_value["data_type"] = input_data_type
                return_value["maximum_time_difference"] = \
                    maximum_time_difference
                return_value["associated_fields"] = associated_fields
                in_situ_colocation = requests.post(
                    request_url,
                    data=json.dumps(return_value),
                    timeout=10
                ).json()

                if 'error' in in_situ_colocation:
                    raise PluginError(
                        "The in situ server responded with an error, this "
                        "is being passed on to you here. The error was: {}"
                    ).format(in_situ_colocation['error'])

                return_value = in_situ_colocation
            except requests.ConnectionError as problem:
                raise PluginError(
                    str(
                        "This felyx instance could not connect the the "
                        "requested associated (in situ) data server. "
                        "Please try this query "
                        "again, without any associated components. Internally, "
                        "the issue was reported as: {}"
                    ).format(problem.message)
                )

            except requests.Timout as problem:
                raise PluginError(
                    str(
                        "The associated data server suffered a timeout, please"
                        " try this query again, or try a smaller "
                        "query. Internally, "
                        "the issue was reported as: {}"
                    ).format(problem.message)
                )
            except PluginError:
                raise

        else:
            return {
                "results": result,
                "arguments": arguments,
                "step_name": "colocation"
            }
